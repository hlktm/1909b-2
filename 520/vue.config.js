const Mock=require("mockjs")


const data=Mock.mock({
    "list|10":[{
        id:"@id",
        title:"@ctitle",

    }]
})


module.exports = {
    devServer: { 
        open: true,
        hot: true ,
        before(app){
            app.get("/list",(req,res)=>{
                res.send(data.list)
            })
        }
    },
}