import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'
import About from '../views/About.vue'
import Index from '../views/Item/Index.vue'
import Classify from '../views/Item/Classify.vue'
import Shopping from '../views/Item/Shopping.vue'
import My from '../views/Item/My.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    redirect: "/home"
  },
  {
    path: '/home',
    component: Home,
    children: [
      {
        path: '/home',
        redirect: "/home/index"
      },
      {
        path: '/home/index',
        component: Index
      },
      {
        path: '/home/classify',
        component: Classify
      },
      {
        path: '/home/shopping',
        component: Shopping
      },
      {
        path: '/home/my',
        component: My
      }
    ]
  },
  {
    path: '/about',
    name: 'About',
    component: About
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
